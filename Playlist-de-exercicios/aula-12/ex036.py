valor = float(input('Qual o valor da casa? '))
sal = float(input('Qual o seu salário mensal? '))
anos = int(input('Em quantos anos você irá pagar? '))
prest = valor / (anos * 12)
if prest <= sal * 0.3:
    print('O empréstimo foi aprovado! Você irá pagar \033[32mR${:.2f}\033[m por mês.'.format(prest))
else:
    print('O empréstimo não foi aprovado, pois a prestação mensal excede 30% do seu salário.')
