num = int(input('Digite um número inteiro: '))
base = int(input('Escolha uma base para conversão. 1- binário, 2- octal, 3-hexadecimal: '))
if base == 1:
    print('{} em binário fica {:b}'.format(num, num))#é possível fazer bin(num)
elif base == 2:
    print('{} em octal fica {:o}'.format(num, num))#oct(num)
elif base == 3:
    print('{} em hexadecimal fica {:X}'.format(num, num))#hex()
else:
    print('Essa opção não existe.')
