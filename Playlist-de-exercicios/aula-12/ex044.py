preco = float(input('Qual o preço do produto? '))
forma = input('Digite 1 para pagar com dinheiro á vista ou 2 para pagar com cartão de crédito: ')
if forma == '1':
    preco = preco * 0.9
    print('Você precisa pagar \033[32mR${:.2f}\033[m.'.format(preco))
elif forma == '2':
    vez = input('Em quantas vezes você irá pagar? ')
    if vez == '1':
        preco = preco * 0.95
        print('Você precisa pagar \033[32mR${:.2f}\033[m.'.format(preco))
    elif vez == '2':
        print('Você precisa pagar \033[32mR${:.2f}\033[m.'.format(preco))
    elif int(vez) > 2:
        preco = preco * 1.2
        print('Você precisa pagar \033[32mR${:.2f}\033[m.'.format(preco))
    else:
        print('Valor inválido.')
else:
    print('Essa opção não existe.')
