reta1, reta2, reta3 = float(input('Digite o comprimento de três retas.\nPrimeira: ')), float(input('Segunda:')), float(input('Terceira:'))
lados = [reta1, reta2, reta3]
lados.sort()
if (lados[0] + lados[1]) > lados[2]:
    print('É possível formar um triângulo com essas três retas.')
    if lados[0] == lados[1] and lados[1] == lados[2]:
        print('Esse triângulo será equilátero!')
    elif (lados [0] == lados[1] and lados[1] != lados[2]) or (lados[0] == lados[2] and lados[2] != lados[1]) or (lados[1] == lados[2] and lados[1] != lados[0]):
        print('Esse triângulo será isósceles!')
    else:
        print('Esse triângulo será escaleno!')
else:
    print('Não é possível formar um triângulo com essas três retas.')
#fiz uma resposta super grande e ineficiente, é muito melhor simplesmente testar o equilatero e escaleno, que são testes menores, e deixar o isosceles como apenan else
#if lados[0] == lados[1] and lados[1] == lados[2]:
#    print('Esse triângulo será equilátero!')
#elif lados[0] != lados[1] and lados[0] != lados[2] and lados[1] != lados[2]:
#    print('Esse triângulo será escaleno!')
#else:
#    print('Esse triângulo será isósceles!')
