peso = float(input('Digite o seu peso em quilogramas: '))
alt = float(input('Digite a sua altura em metros: '))
imc = peso / (alt ** 2)
if imc < 18.5:
    print('Você está abaixo do peso.')
elif imc < 25:
    print('Você está no peso ideal.')
elif imc < 30:
    print('Você está em condição de sobrepeso.')
elif imc < 40:
    print('Você está com obesidade.')
else:
    print('Você está com obesidade mórbida.')
