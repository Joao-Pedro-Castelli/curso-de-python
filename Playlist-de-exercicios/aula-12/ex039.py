import datetime

resp = input('Você é homem?(S/N) ')
if resp == 'S':
    data = datetime.datetime.now()
    ano = int(data.strftime("%Y"))
    idade = ano - int(input("Em que ano você nasceu? "))
    if idade < 18:
        print('Faltam {} anos para você se alistar.'.format(18 - idade))
    elif idade == 18:
        print('Está na hora de você se alistar!')
    else:
        print('Já passou {} anos desde que você teve que se alistar.'.format(idade - 18))
elif resp == 'N':
    print('Você não precisa se apresentar para o alistamento militar.')
else:
    print('Resposta inválida!')
