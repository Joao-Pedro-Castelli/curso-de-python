num1 = float(input('Digite um número: '))
num2 = float(input('Digite mais um número: '))
if num2 > num1:
    print('O segundo valor é maior que o primeiro, ou seja, {} > {}'.format(num2, num1))
elif num1 > num2:
    print('O primeiro valor é maior que o segundo, ou seja, {} > {}'.format(num1, num2))
else:
    print('Os dois valores são iguais. {} = {}'.format(num1, num2))
