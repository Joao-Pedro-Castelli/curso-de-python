num = int(input('Digite um número: '))
dobro = num * 2
triplo = num * 3
raizq = num ** (1/2)
print('O dobro de {} é {}.\nO triplo é {}.\nE a raiz quadrada dele é {:.3f}'.format(num, dobro, triplo, raizq))
