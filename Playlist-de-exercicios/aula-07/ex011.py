larg, altu = float(input('Insira a largura da parede em metros: ')), float(input('Insira a altura da parede em metros: '))
area = larg * altu
print('Considerando que 1 litro de tinta pinta 2 metros quadrados, será necessário {:.1f} litros para {:.1f} m²'.format((area/2), area))
