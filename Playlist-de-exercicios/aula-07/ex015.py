dias = float(input('Por quantos dias o carro foi alugado? '))
km = float(input('Quantos quilômetros o carro percorreu nesse tempo? '))
custo = dias * 60 + km * 0.15
print('O custo do aluguel ficou como R${:.2f}'.format(custo))