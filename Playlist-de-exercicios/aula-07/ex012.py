orig = float(input('Qual o preço da sua compra? R$'))
desc = orig * 0.95
print('Parabéns! Você é o 100000º cliente da nossa loja e receberá 5% de desconto nessa compra.\nPreço atual: R${:.2f}'.format(desc))