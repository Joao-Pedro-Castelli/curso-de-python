from math import sin, cos, tan, radians

ang = float(input('Digite um ângulo em graus: '))
rad = radians(ang)
print('O ângulo {:.2f}rad tem o seno {:.2f}, cosseno {:.2f} e tangente {:.2f}'.format(rad, sin(rad), cos(rad), tan(rad)))
