from random import shuffle

aluno1 = input('Qual o nome do primeiro aluno? ')
aluno2 = input('Qual o nome do segundo aluno? ')
aluno3 = input('Qual o nome do terceiro aluno? ')
aluno4 = input('Qual o nome do quarto aluno? ')
ordem = [aluno1, aluno2, aluno3, aluno4]
shuffle(ordem)
print('A ordem de apresentação sorteada foi {}, depois {}, depois {} e, por último, {}.'.format(ordem[0], ordem[1], ordem[2], ordem[3]))
