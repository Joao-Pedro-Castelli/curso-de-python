from math import hypot

cop, cad = float(input('Digite o comprimento do cateto oposto em centímetros:')), float(input('Digite o comprimento do adjacente em cm: '))
hip = hypot(cop, cad)
print('A hipotenusa do triangulo retângulo de lados {}cm e {}cm mede {:.2f}cm'.format(cop, cad, hip))
