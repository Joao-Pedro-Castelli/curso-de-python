palavra = input('Digite algo: ')
print('O tipo primitivo desse valor é {} \nSó tem espaços? {} \nÉ um número? {} \nÉ alfabético? {} \nÉ alfanumérico? {} \nEstá em maiúsculas? {} \nEstá em minúsculas? {} \nEstá capitalizada? {}'.format(type(palavra), palavra.isspace(), palavra.isnumeric(), palavra.isalpha(), palavra.isalnum(), palavra.isupper(), palavra.islower(), palavra.istitle()))
