tex = input('Digite uma frase: ').upper()
quant = tex.count('A')
print('A letra "A" aparece {} vezes na frase.'.format(quant))
lista = tex.split('A')
print('A primeira instância dessa vogal é na posição [{}]'.format(len(lista[0])))
print('A última vez que "A" aparece é na posição [{}]'.format( len(tex) - len(lista[quant]) - 1 ))
 #print('A última vez que "A" aparece é na posição [{}]'.format(tex.rfind('A')))
 #Se eu tivesse usado o rfind(), seria mais fácil. Lembresse, funções string com r no começo leem da direita pra esquerda