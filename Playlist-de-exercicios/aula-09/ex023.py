#num = input('Digite um número de até quatro dígitos: ')
#l = len(num)
#print('Milhar: {:>2}\nCentena: {:>1}\nDezena: {:>2}\nUnidade: {:>1}'.format(num[l - 4], num[l - 3], num[l - 2], num[l - 1]))

num = int(input('Digite um número de até 4 dígitos: '))
milhar = num // 1000 % 10
centena = (num // 100) - (num // 1000 * 10)
dezena = (num // 10) - ((num // 100) * 10)
unidade = (num % 10)
print(milhar, centena, dezena, unidade)