maior = 0
menor = 123456
for i in range(0, 5):
    if i == 1:
        a = 'uma'
    else:
        a = 'outra'
    pes = float(input('Digite o peso de {} pessoa em KG: '.format(a)))
    if float.is_integer(pes):
        pes = int(pes)
    if pes > maior:
        maior = pes
    if pes < menor:
        menor = pes
else:
    print('O maior peso foi {}KG e o menor foi {}KG'.format(maior, menor))
