n = 0
soma = 0
for i in range(1, 496, 2):
    if i % 3 == 0:
        print(i, end=' ')
        soma += i
        n += 1
    if n == 30:
        print()
        n = 0
print('\nA soma de todos esses números é igual a {}'.format(soma))
