print('-'*38, '\n{:^}'.format('Vamos fazer uma progressão aritmética!\n') + '-'*38)
A1, R = float(input('Qual o primeiro termo? ')), float(input('Qual a razão? '))
for i in range(0, 10):
    An = A1 + R * i
    if float.is_integer(An):
        An = int(An)
    print(An, end=' > ')
else:
    print('finalizado!')
