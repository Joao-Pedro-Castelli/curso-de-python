num = int(input('Tabuada de um número inteiro: '))
for i in range(0, 11):
    res = i * num
    #print('{} X {} =  {}'.format(num, i, res) if i != 10 else '{} X {}=  {}'.format(num, i, res))
    print('{} X {:2} = {}'.format(num, i, res))
