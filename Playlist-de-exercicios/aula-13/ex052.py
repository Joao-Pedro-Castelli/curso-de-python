num = int(input('Digite um número inteiro positivo: '))
div = 0
if num > -1:
    if num == 0 or num == 1:
        print('Não é primo, nem composto.')
    elif num % 2 == 0:
        print('Não é primo.')
    else:
        for i in range(num, 1, -2):
            if num % i == 0:
                div += 1
        else:
            if div > 1:
                print('Esse número é divisível por {} números.'.format(div + 1))
            else:
                print('O número é primo.')
else:
    print('Não')
