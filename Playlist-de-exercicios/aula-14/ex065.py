soma, resposta, c = 0, 'S', 1#soma will receive the sum of the typed numbers, resposta is to know if the user wants to continue and c will count the ammount of numbers typed
num = float(input('Digite um número: '))
maior, menor = num,  num#maior is the biggest number typed (currently the only number) and menor is the smallest (same)
soma += num 
while resposta == 'S':#while the user answers that he wants to continue
    v = 0#variable to check if the input is right, needs to be reseted
    num = float(input('Digite outro número: '))
    c += 1#counter
    if num > maior:
        maior = num
    if num < menor:
        menor = num
    soma += num
    while v == 0:
        resposta = input('Você quer digitar outro número?(S/N) ').upper().strip()
        if resposta == 'S' or resposta == 'N':
            v = 1
        else:
            print('Resposta inválida!')
print('A média do valor dos números digitados é {}, o maior número foi {} e o menor foi {}'.format(soma / c, maior, menor))
