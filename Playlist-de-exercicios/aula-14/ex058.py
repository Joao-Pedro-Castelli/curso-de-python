from random import randint

c = r = 1
num = randint(0, 10)
tentativa = int(input('Eu pensei num número entre 0 e 10. Tente adivinhá-lo. '))
if num == tentativa:
    print('Parabéns, você acertou de primeira!')
else:
    while r != 2:
        if num > tentativa:
            tentativa = int(input('O número é maior, tente novamente. '))
        else:
            tentativa = int(input('O número é menor, tente novamente. '))
        c += 1
        if tentativa == num:
            r += 1
    print('Você acertou em {} tentativas'.format(c))
