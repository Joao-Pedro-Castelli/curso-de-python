atual, anterior, proximo, c, somar = 1, 0, 0, 0, 0
print(10*'-', 'Sequência de Fibonacci', '-'*10)
n = int(input('Quantos números você quer mostrar? '))
print(anterior, end=' -> ')
while c < n - 1:
    #rint(atual, end=' -> ')
    c += 1
    proximo = atual + anterior
    anterior = atual
    atual = proximo
    somar += atual/anterior
#print('Fim!')
print(somar/n)
