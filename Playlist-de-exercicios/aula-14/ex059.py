from os import system
from time import sleep
r = 0
num1, num2 = int(input('Digite um número: ')), int(input('Digite outro número: '))
while r != 5:
    print("""------Menu----------
[1]Somar
[2]Multiplicar
[3]Maior
[4]Novos números
[5]Sair do programa
--------------------""")
    r = int(input('O que você quer fazer? '))
    system('clear')
    if r == 1:
        print('{} + {} = {}\n'.format(num1, num2, num1 + num2))
    elif r == 2:
        print('{} X {} = {}\n'.format(num1, num2, num1 * num2))
    elif r == 3:
        if num1 > num2:
            txt = '{} é maior do que {}'.format(num1, num2)
        elif num1 == num2:
            txt = '{} é igual a {}'.format(num1, num2)
        else:
            txt = '{} é maior do que {}'.format(num2, num1)
        print('{}\n'.format(txt))
    elif r == 4:
        num1, num2 = int(input('Digite um número: ')), int(input('Digite outro número: '))
    elif r != 5:
        print('Entrada inválida!\n')
print('Volte sempre')
