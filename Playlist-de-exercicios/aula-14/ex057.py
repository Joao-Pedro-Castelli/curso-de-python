sex = ''
sex = input('Qual o seu sexo? [H/M]').upper()
sex_clean = sex.strip()      #tira possíveis espaços que a pessoa colocou, ou seja, aceita o string mesmo com espaços
while not (sex_clean == 'H')^(sex_clean == 'M'):
    print('Resposta inválida')
    sex = input('Qual o seu sexo? [H/M]').upper()
    sex_clean = sex.strip()
print('Você é homem, entendido.' if sex_clean == 'H' else 'Você é mulher, entendido.')
