a1, r = float(input('Qual o primeiro termo? ')), float(input('Qual a razão? '))
if float.is_integer(a1):
    a1 = int(a1)
    print(a1, end=' - ')
else:    
    print('{:.2f}'.format(a1), end=' - ')
c = 1
while c < 10:
    a1 += r
    if float.is_integer(a1):
        a1 = int(a1)
        print(a1, end=' - ')
    else:
        print('{:.2f}'.format(a1), end=' - ')
    c += 1
print('Finalizado!')
