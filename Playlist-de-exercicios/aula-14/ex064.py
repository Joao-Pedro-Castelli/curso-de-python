print('Digite números e vou somá-los. 999 para parar o programa.')
resposta, sumv, count = 0, 0, -1
while resposta != 999:#o loop vai somar números a var sumv e vai lembrar de quantos números foram contados
    sumv += resposta#se o usuario colocar 999, vai sair do loop
    count += 1#count begins with -1, because it will sum 1 when the user types 999, but it won't count.
    resposta = float(input('Digite um número: '))
if float.is_integer(sumv):#formatting from float to int if possible
    sumv = int(sumv)
else:
    sumv = '{:.2f}'.format(sumv)
print('A soma desses {} números resultou em {}'.format(count, sumv))
