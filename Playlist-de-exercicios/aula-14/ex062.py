c, l = 0, 10
print(10*'-', ' Progressão aritmética ', 10*'-')
a1, r = float(input('Qual será o primeiro número? ')), float(input('Qual será a razão? '))
while l != 0:
    while c < l:
        print(' -> ', end='')
        if float.is_integer(a1):                               #{ apenas printa o número da sequencia
            print('{}'.format(int(a1)), end='')                   #  ve se pode ser um integer 
        else:                                                  #  para poder formatar direito
            print('{:.2f}'.format(a1), end='')                   #} dai coloca o proximo termo em a1 e aumenta o contador do while
        a1 += r
        c += 1
    l, c = int(input(' -> Pausa \nQuantos números mais você quer? Digite 0 para terminar. ')), 0 #recebe quantos números mais vai printar em $l e reseta o contador $c
print('Finalizado!')
