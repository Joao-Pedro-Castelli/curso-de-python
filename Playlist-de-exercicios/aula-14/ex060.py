fat = 1
x = int(input('Qual número você quer calcular o fatorial de? '))
if x < 0:
    print('Não existe fatorial de números negativos.')
elif x == 0:
    print('0! = 0')
else:
    c = x
    #for c in range(x, 1, -1):
        #fat = c * fat
    while c > 1:
        print(c, end=' X ')
        fat = c * fat
        c -= 1
    print('1 = {} ({}! = {})'.format(fat, x, fat))
