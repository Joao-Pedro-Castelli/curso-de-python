Nessa playlist estarão todos os exercícios que o professor Gustavo Guanabara passou em:
https://youtube.com/playlist?list=PLHz_AreHm4dm6wYOIW20Nyg12TAjmMGT-

Do que cada exercício se trata:

Aula 4 - Comandos básicos.
{
001- Olá, Mundo!
002- Usando o input()
}
Aula 6 - Variáveis
{
003- Manipulando o tipo primitivo de uma variável.
004- Usando funções para reconhecer características de um String.
}
Aula 7 - Operadores aritméticos
{
005- Introdução a operações aritméticas.
006- Usando operadores aritméticos.
007- Média aritmética.
008- Conversão de metros para centímetros e milímetros.
009- Tabuada de um número.
010- Conversão de moedas.
011- Cálculo da área de uma superfície.
012- Porcentagem / desconto
013- Porcentagem / aumento
014- Celsius para Fahrenheit.
015- Calculando preço por meio de 2 parâmetros.
}
Aula 8 - Introdução à módulos e bibliotecas (import)
{
016- Uso da função math.trunc()
017- Cálculo do comprimento da hipotenusa.
018- Seno, cosseno e tangente de um ângulo.
019- Sorteand um aluno / random.choice()
020- Embaralhando uma lista.
021- Reprodução de um arquivo MP3.
}
Aula 9 - Manipulação de String
{
022- Usando funções da classe String.
023- Separando os algarismos de um número.
024- Verifica se a String começa com uma certa sub-string.
025- Verifica se há uma sub-string na String.
026- Conta a quantidade de ocorrência de uma letra e encontra onde ela aparece pela primeira e última vezes.
027- Lê um nome completo e diz qual o primeiro e o último.
}
Aula 10 - Estruturas de condição
{
028- Jogo de adivinhação simples.
029- Se o carro ultrapassar o limite de velocidade, calcular a multa.
030- Verifica se o número é par ou ímpar.
031- Calcule o preço de uma viagem, verificando se ela é de mais de 200km ou não.
032- Verifica se o ano do input e o atual são bissexto.
033- Recebe três números e retorna quais são os maior e menor.
034- Calcular o aumento do salário, 10% para quem ganha > x e 15% para <= x.
035- Retorna se três retas conseguem formar um triângulo ou não.
}
Aula 12 - Condições aninhadas
{
36- Aprovação de empréstimo, uso de float(input()) e if else.
37- Converter um número inteiro para outra base numérica, usando {:b}, {:o} e {:X}.format()
38- Recebe dois ints e fala qual o maior ou se os dois são iguais.
39- Calcula a idade e fala quando falta para se alistar ou quanto tempo passou desda época. Usou datetime.
40- Calcula a média de um aluno e diz se ele foi reprovado, recuperação ou aprovado. Usa aritmética.
41- Verifica em que faixa de idade a pessoa está(mirim, infantil, junior, sênior ou master). Usa elif e aninhação.
42- Remaster do exer 35, agora mostrando que tipo de triângulo é. Aninhação e matemática.
43- Calcula o IMC da pessoa.
44- Calcular o preço de um produto, considerando desconto a vista e juros.
45- Jogo de Jokenpô.
}
Aula 13 - Laço de repetição for
{
46- Contagem regressiva. Usa for e sleep()
47- Todos os números pares entre 1 e 50. Usa for e print(end=' ') e %
48- Soma dos números impares múltiplos de 3 entre 1 e 500. Uso do for, range(x, y, z) e %
49- Refatoração do exercício 9. Uso for e format
50- Recebe seis números e faz a soma dos que forem pares. Uso % e +=
51- Recebe o A1 e o R de uma PA e mostra os 10 primeiros termos. Uso '{:^}'format(), end='' e ''*10
52- Verifica se um número é primo ou não.
53- Verifica se uma frase é um palíndromo. Uso reversed(), join(), replace()
54- Recebe 7 anos de nascimento e diz quais são maiores de 21. Uso datetime e math
55- Recebe cinco valores e diz qual o menor e qual o maior.
56- Recebe nome, idade e sexo de 4 pessoas e diz alguns dados. Uso float.is_integer()
}
Aula 14 - Laço de repetição while
{
57- Recebe um string e verifica se é valido. Usa while e xor
58- Aprimoramento do 28 usando o while
59- Recebe dois números e faz algo com eles, continua fazendo isso até o usuário sair do programa.
60- Calcula o fatorial de um dado número. Uso de while e math
61- Refatoração do exer 51, usando while. Usa formatação
62- Aprimoramento do código anterior, dando mais controle ao usuário
63- Recebe input n e mostra os n primeiros nums da sequencia de fibonacci
64- Recebe numeros até o mestre querer parar e soma-os
65- Recebe nums, perguntando se quer continuar a digitar, e dá a média, o maior e o menor valores digitados. Usa validação em loop
}
Aula 15 - 
