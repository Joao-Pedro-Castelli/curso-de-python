linha = '='*20
print(f'{linha}\n     Banco CEV\n{linha}')
valorsaq = int(input('Qual valor você quer sacar? R$'))
ced50 = valorsaq // 50
resto = valorsaq % 50
ced20 = resto // 20
resto = resto % 20
ced10 = resto // 10
resto = resto % 10
ced1 = resto
print(f'''Total de {ced50} cédulas de 50 reais.
Total de {ced20} cédulas de 20 reais.
Total de {ced10} cédulas de 10 reais.
Total de {ced1} cédulas de 1 real.''')
