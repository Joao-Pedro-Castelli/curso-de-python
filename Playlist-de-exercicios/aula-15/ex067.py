n = 0
while 1 == 1:
    n = int(input('Digite um número e mostrarei a tabuada dele (negativo para o loop): '))
    if n < 0:
        break
    print('-'*20)
    for i in range(1, 10):
        print(f'{n} x {i} = {n*i}')
    print('-'*20)
print('Programa encerrado')
