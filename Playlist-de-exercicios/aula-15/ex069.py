continuar = True
maiord18 = homem = mulhermenosd20 = 0
print('='*5, 'Começo Do Programa', '='*5)
while continuar:
    print('-'*20, f'\nCadastre uma pessoa\n', '-'*20)
    while True:
        idade = input('Idade: ')
        if idade.isdigit():
            idade = int(idade)
            break
    while True:
        sexo = input('Sexo[M/F]: ').upper()
        if sexo == 'M' or sexo =='F':
            break
    print('-'*20)
    if idade > 18:
        maiord18 += 1
    if sexo == 'M':
        homem += 1
    elif idade < 20:
        mulhermenosd20 += 1
    while True:
        resposta = input('Quer continuar? [S/N] ').upper()
        if resposta == 'S':
            continuar = True
            break
        elif resposta == 'N':
            continuar = False
            break
print('='*5, 'Fim do programa', '='*5)
print(f'Total de pessoas com mais de 18 anos: {maiord18}')
print(f'Ao todo temos {homem} homens cadastrados')
print(f'E temos {mulhermenosd20} mulheres com menos de 20 anos cadastradas')
