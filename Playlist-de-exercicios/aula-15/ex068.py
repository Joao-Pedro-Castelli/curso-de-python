from random import randint

print('=-'*12)
print('Vamos jogar par ou ímpar')
cont = player = bot = res = bolean = 0
while True:
    print('=-'*12)
    bolean = input('Você vai ser par ou ímpar?(P/I) ').upper()
    if bolean != 'I' and bolean != 'P':
        print('Sujão!')
        break
    bot = randint(0, 10)
    player = int(input('Diga um valor entre 0 e 10: '))
    if player < 0 or player > 10:
        print('Sujão!')
        break
    print('-'*24)
    print(f'Você jogou {player} e eu joguei {bot}. O resultado é {player + bot}', end=' ')
    print('(par)' if (player + bot) % 2 == 0 else '(impar)')
    if (player + bot) % 2 == 0:
        res = 'P'
    else:
        res = 'I'
    if bolean != res:
        print('-'*24)
        print('Você perdeu')
        break
    else:
        print('-'*24)
        print('Você venceu')
        print('Vamos jogar novamente... ')
        cont += 1
print('=-'*12)
print(f'Game Over! Você venceu {cont} vezes.')
