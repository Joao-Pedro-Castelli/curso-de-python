menor = float('Inf')
precototal = 0
maisdmil = 0
print('-'*26, '\n     Loja Super Baratão\n' + '-'*26)
while True:
    while True:
        nome = input('Nome do produto: ')
        if not nome.replace('.', '').replace(',', '').isnumeric() and nome[0] != '-':
            break
    while True:
        preco = input('Preço: R$')
        if preco.replace('.', '').replace(',', '').isnumeric():
            preco = float(preco.replace(',', '.'))
            break
    if preco < menor:
        menor = preco
        nomemenor = nome
    if preco > 1000.0:
        maisdmil += 1
    precototal += preco
    while True:
        resposta = input('Você quer continuar? [S/N] ').upper()
        if resposta == 'S' or resposta == 'N':
            break
    if resposta == 'N':
        break
print('{:-^30}'.format('Fim do programa'))
print(f'O total da compra foi R${precototal:.2f}')
print(f'Quantidade de produtos que custaram mais de mil reais: {maisdmil}')
print(f'O produto mais barato foi {nomemenor}, que custou R${menor:.2f}')
