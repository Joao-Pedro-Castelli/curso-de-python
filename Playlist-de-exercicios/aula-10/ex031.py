dist = float(input('Qual a distância (Km) da viagem que fará? '))
if dist <= 200:
    preco = dist * 0.5 #preco = dist * 0.5 if dist <= 200 else preco = dist * 0.45
else:
    preco = dist * 0.45
print('O preço da passagem é de R${:.2f}'.format(preco))
