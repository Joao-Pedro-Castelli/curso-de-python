from random import randint

num = randint(0, 5)
guess = int(input('Tente adivinhar o número que estou pensando entre zero e cinco. '))
print('Você acertou! ' if num == guess else 'Você errou. ')
print('Eu pensei no número {}'.format(num))
