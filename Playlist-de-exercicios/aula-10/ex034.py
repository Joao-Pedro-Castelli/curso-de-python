sal = float(input('Digite o seu salário: R$'))
if sal > 1250:
    sal = sal * 1.1
else:
    sal = sal * 1.15
print('O seu novo salário é de R${:.2f}'.format(sal))
