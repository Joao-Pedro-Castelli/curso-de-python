import datetime

ano_user = int(input('Digite um ano qualquer: '))
ano_atual = datetime.date.today().year
bol_user = ano_user % 4 == 0 and (ano_user % 100 != 0 or ano_user % 400 == 0)
bol_atual = ano_atual % 4 == 0 and (ano_atual % 100 != 0 or ano_atual % 400 == 0)
if bol_user:
    txt = '{} é um ano bissexto.\n'.format(ano_user)
else:
    txt = '{} não é um ano bissexto.\n'.format(ano_user)
if bol_atual:
    txt = txt + 'O ano atual é bissexto.'
else:
    txt = txt + 'O ano atual não é bissexto.'.format(ano_user)
print(txt)
