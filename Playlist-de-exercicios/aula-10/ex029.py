vel = float(input('Qual a velocidade do carro? '))
print('Você está no limite.' if vel <= 80 else 'Você ultrapassou o limite de velocidade. Será multado!')
if vel > 80:
    print('A sua multa é de R${:.2f}.'.format(7 * (vel - 80)))
