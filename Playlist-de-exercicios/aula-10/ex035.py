reta1, reta2, reta3 = float(input('Digite o comprimento de três retas.\nPrimeira: ')), float(input('Segunda:')), float(input('Terceira:'))
lados = [reta1, reta2, reta3]
lados.sort()
if (lados[0] + lados[1]) > lados[2]:
    print('É possível formar um triângulo com essas três retas.')
else:
    print('Não é possível formar um triângulo com essas três retas.')
