#o propósito desse programa é calcular dígitos de pi usando números aleatórios e geometria
#a ideia veio do vídeo no Youtube 'Can you solve my favorite interview question? (math + cs)' do Joma Tech
from random import random
dentro = 0          #número de pontos dentro do círculo
total = 10 ** 12     #número total de pontos no plano
for c in range(0, total):
    a = random()        #par de coordenadas
    b = random()        #ponto no plano = (a, b)
    distancia = (a ** 2) + (b ** 2)     #distancia do ponto (0, 0)
    if distancia <= 1:                  #se estiver dentro ou na circunferencia
        dentro += 1
aproxpi = (4 * dentro) / total
print(aproxpi)
