tarray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 26]
tortoise = tarray[tarray[0]]
tortoise_index = tarray[0]
hare = tarray[tarray[tarray[0]]]
hare_index = tarray[tarray[0]]

while tortoise_index != hare_index:
    tortoise_index = tortoise
    tortoise = tarray[tortoise]
    hare_index = tarray[hare]
    hare = tarray[tarray[hare]]

start = tarray[0]
start_index = 0

while start_index != tortoise_index:
    start_index = start
    start = tarray[start]
    tortoise_index = tortoise
    tortoise = tarray[tortoise]

print('O número repetido é {}'.format(start_index))
